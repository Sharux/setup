#!/bin/bash

# Update
sudo pacman -Syyu --noconfirm

# Xorg
sudo pacman -S --noconfirm xorg-xrandr xorg-server xorg-xinit libxft libxinerama imlib2

# Font
sudo pacman -S --noconfirm ttf-jetbrains-mono ttf-font-awesome # ttf-joypixels gnu-free-fonts noto-fonts ttf-hack

# Drivers
sudo pacman -S --noconfirm nvidia nvidia-settings

# Audio
sudo pacman -S --noconfirm pulseaudio pavucontrol

# Theme
sudo pacman -S --noconfirm breeze breeze-gtk

# systray icons
# sudo pacman -S --noconfirm volumeicon network-manager-applet networkmanager networkmanager-openconnect networkmanager-openvpn networkmanager-pptp networkmanager-vpnc trayer

# Other
sudo pacman -S --noconfirm archlinux-keyring fish starship neovim base-devel alacritty  fuse ntfs-3g nitrogen fzf ripgrep the_silver_searcher fd gvfs lxsession lxappearance nodejs rofi exfat-utils xarchiver pcmanfm man

cd ~/
git clone https://gitlab.com/Sharux/dotfiles
git clone https://gitlab.com/Sharux/suckless
git clone https://github.com/dylanaraps/pfetch && mv pfetch ~/suckless/

rm -rfv .config
cd dotfiles
mv .XColors    ~/
mv .config     ~/
mv .surf       ~/
mv .Xresources ~/
mv .bashrc     ~/
mv .gtkrc-2.0  ~/
mv .xinitrc    ~/
mv .zshenv     ~/
mv .zshrc      ~/
mv xorg.conf   ~/
cd

cd suckless/
cd dmenu    && sudo make clean install && cd ..
cd dwm      && sudo make clean install && cd ..
cd slstatus && sudo make clean install && cd ..
cd st       && sudo make clean install && cd ..
cd surf     && sudo make clean install && cd ..
cd sxiv     && sudo make clean install && cd ..
cd tabbed   && sudo make clean install && cd ..
cd pfetch   && sudo make install && cd ~/

# AUR helper
git clone https://aur.archlinux.org/yay
cd yay && makepkg --noconfirm  -si && cd
yay -S brave-bin

# picom
git clone https://github.com/jonaburg/picom
cd picom && makepkg --noconfirm -si && cd

# Fonts
cd .local/share/
git clone https://github.com/ryanoasis/nerd-fonts
cd nerd-fonts
./install.sh
cd

# xorg.conf
cd /etc/X11
rm -rfv xorg.conf
cd
sudo mv xorg.conf /etc/X11/

# ls /usr/share/zoneinfo/
# sudo rm /etc/localtime
# sudo ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime

rm -rfv yay picom dotfiles


